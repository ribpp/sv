CC=gcc
FLAGS=-O2 -Wall -Wpedantic -std=c99

all: test

test:
	mkdir --parents bin
	$(CC) test.c -o bin/test $(FLAGS)

clean:
	$(RM) -r bin/
