#include <assert.h>

#define SV_IMPLEMENTATION
#include "sv.h"

#define ASSERT_EQ(a, b) assert(sv_eq(a, b))

int main(void)
{
	// Constructors
	{
		ASSERT_EQ(SV("hello"), sv_from_parts("hello", 5));
		ASSERT_EQ(SV("hello"), sv_from_cstr("hello"));

		String_View static_string_view = SV_STATIC("hello");
		ASSERT_EQ(SV("hello"), static_string_view);
	}

	// Trim
	{
		ASSERT_EQ(sv_trim_left(SV("    hi")), SV("hi"));
		ASSERT_EQ(sv_trim_right(SV("hi   ")), SV("hi"));
		ASSERT_EQ(sv_trim(SV("      hi   ")), SV("hi"));
	}

	// Starts With & Ends With
	{
		assert(sv_starts_with(SV("hello, world"), SV("hello")));
		assert(sv_ends_with(SV("hello, world"), SV("world")));
	}
}
