#ifndef SV_H_
#define SV_H_

#include <ctype.h>
#include <stdbool.h>
#include <string.h>

#ifndef SVDEF
#define SVDEF
#endif

typedef struct {
    const char *data;
    size_t count;
} String_View;

#define SV(cstr) sv_from_parts(cstr, strlen(cstr))
#define SV_STATIC(cstr) \
		{ \
		    .data = cstr, \
			.count = strlen(cstr) \
		}

#define SV_NULL sv_from_parts(NULL, 0)

// Usage: printf(SV_Fmt"\n", SV_Arg(sv));
#define SV_Fmt "%.*s"
#define SV_Arg(sv) (int)(sv.count), sv.data

SVDEF String_View sv_from_parts(const char *cstr, size_t count);
SVDEF String_View sv_from_cstr(const char *cstr);

SVDEF String_View sv_trim_left(String_View sv);
SVDEF String_View sv_trim_right(String_View sv);
SVDEF String_View sv_trim(String_View sv);

SVDEF bool sv_eq(String_View a, String_View b);
SVDEF bool sv_starts_with(String_View sv, String_View expected_prefix);
SVDEF bool sv_ends_with(String_View sv, String_View expected_suffix);

#endif // SV_H_

#ifdef SV_IMPLEMENTATION

SVDEF String_View sv_from_parts(const char *cstr, size_t count)
{
    return (String_View) {
        .data = cstr,
        .count = count
    };
}

SVDEF String_View sv_from_cstr(const char *cstr)
{
    return sv_from_parts(cstr, strlen(cstr));
}

SVDEF String_View sv_trim_left(String_View sv)
{
    size_t i = 0;
    while (i < sv.count && isspace(sv.data[i])) {
        i += 1;
    }

    return sv_from_parts(sv.data + i, sv.count - i);
}

SVDEF String_View sv_trim_right(String_View sv)
{
    size_t i = 0;
    while (i < sv.count && isspace(sv.data[sv.count - i - 1])) {
        i += 1;
    }

    return sv_from_parts(sv.data, sv.count - i);
}

SVDEF String_View sv_trim(String_View sv)
{
    return sv_trim_right(sv_trim_left(sv));
}

SVDEF bool sv_eq(String_View a, String_View b)
{
    if (a.count != b.count) {
        return false;
    } else {
        return memcmp(a.data, b.data, a.count) == 0;
    }
}

SVDEF bool sv_starts_with(String_View sv, String_View expected_prefix)
{
    if (expected_prefix.count > sv.count) {
        return false;
    }

    String_View real_prefix = sv_from_parts(sv.data, expected_prefix.count);
    return sv_eq(real_prefix, expected_prefix);
}

SVDEF bool sv_ends_with(String_View sv, String_View expected_suffix)
{
    if (expected_suffix.count > sv.count) {
        return false;
    }

    String_View real_suffix = sv_from_parts(sv.data + (sv.count - expected_suffix.count), expected_suffix.count);
    return sv_eq(real_suffix, expected_suffix);
}

#endif // SV_IMPLEMENTATION

